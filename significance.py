#!/usr/bin/env python3
"""Simple script to run the significance test from new_stats over
two files with one-float-per-line"""

import numpy as np
import new_stats
import sys

if __name__ == "__main__":
    samples1 = np.loadtxt(sys.argv[1], dtype=np.dtype(np.float32))
    samples2 = np.loadtxt(sys.argv[2], dtype=np.dtype(np.float32))
    p, flawed, name = new_stats.determine_significance(samples1, samples2)
    diff_percent = np.average(samples2) / np.average(samples1)
    if flawed: print("!!! FLAWED !!!")
    print("{0:.2f}% (n = {1}, p = {2}, test = {3})".format(100 * diff_percent,
        min(len(samples1), len(samples2)), p, name))
