#!/bin/bash

function print_bandwidth()
{
	echo $1 | awk -F "\"*,\"*" '{printf "%.2f %s/s\n", $1/$4*1000000000, $2}'
}

function measure_average()
{
	local temp=$(mktemp "${TMPDIR:-/tmp/}$(basename 0).XXXXXXXXXXXX")
	local app=$1

	sudo perf stat -e uncore_imc/data_writes/,uncore_imc/data_reads/ -x, -a timeout 10 ${app} > ${temp} 2>&1
	writes=$(sed -n 1p ${temp})
	reads=$(sed -n 2p ${temp})

	echo -n "Read bandwidth: "
	print_bandwidth "$reads"
	echo -n "Write bandwidth: "
	print_bandwidth "$writes"

	rm ${temp}
}

function measure_display_bandwidth()
{
	echo no
	#sudo intel_reg read 0x42404 && $(timeout 10 ./kmscube); sudo intel_reg read 0x42404
}

function measure_peak()
{
	local r_temp=$(mktemp "${TMPDIR:-/tmp/}$(basename 0).XXXXXXXXXXXX")
	local w_temp=$(mktemp "${TMPDIR:-/tmp/}$(basename 0).XXXXXXXXXXXX")
	local app=$1

	# easiest way to sort this is to run the thing twice. In the future we
	# can be smarter
	sudo perf stat -x, -I 100 -e uncore_imc/data_reads/ -a timeout 10 ${APP} > ${r_temp} 2>&1
	sudo perf stat -x, -I 100 -e uncore_imc/data_writes/ -a timeout 10 ${APP} > ${w_temp} 2>&1

	echo -n "Peak Read bandwidth: "
	echo -n `sort -t, -rnk2 $r_temp | head -n1 | awk -F',' '{print $2}'`
	echo " MiB"
	echo -n "Peak Write bandwidth: "
	echo -n `sort -t, -rnk2 $w_temp | head -n1 | awk -F',' '{print $2}'`
	echo " MiB"

	rm ${r_temp}
	rm ${w_temp}
}

APP="${@:-./kmscube}"

measure_average "${APP}"
measure_peak "${APP}"
